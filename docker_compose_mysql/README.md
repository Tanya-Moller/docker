# Building simple web and MySQL with docker compose

This repo will build from 2 docker files in root to create a working docker compose.

### Prerequisites
- Docker


## Objective

Have a simple httpd page that lists the existing tables on a DB.
For this we'll need:
- httpd container
- mysql container
  - Create a table in here
  - Give it a password and other environment variables.
  - Find a way of getting the data into a txt file


## Kubernetes

Let's understand how it works. 
First you have your main machines:

- Master x
- Worker nodes(x2 or more)
- pods (launching on worker node)



#### **Master**

you might have two of these in case on dies.
- naming
- networking
- placement (where did I launch that pod, where it it running)

For the worker nodes to have full internet access, its best o have a HA proxy pod. The IP of this HA proxy will the an entry point to the cluster. 
You'll then assign a Load balancer (ELB) to point to it and also above set a DNS with a will card point to it. 

and 

#### **WorkerNodes**

Machine running docker that lunches Pods

#### **POD**

Get launched on worker nodes. Podes are:
- configuration of docker containers
- Health check 
- how many containers and other atribures 


**name space** in k8 is kind like a VPC

**service** is like a proxy / load balancer

etcd - is service discovery. Basically a DB, key:pair that keeps that of what got lunched and where


## Installing K8

Best way to start a cluster is by using ansible kubespray.

You need ansible and 3 host machines.

You'll specify which one will be the master and the worker nodes. 

## Main way of communicating 

- kubectl
- (you might look at) helm

## If you don't want to lock your self to cloud provider - you build your own cluster. 


## Making our own cluster minimum requierments

- minimum 16ram 
- 3ta.xlarge x3 with 30gb (k8master + 2 k8workerss)
- t2.micro with 20gb (k8bastion - to kubebspray and ansible)


### Install using kubspray

We are going to install using kube spray:

- https://github.com/kubernetes-sigs/kubespray#requirements

- Start machines
- Allow all traffic from subnet into all machines
- Exchange keys
- Install requirements from official documentation
- Run Ansible playbooks to Kubespray
- 