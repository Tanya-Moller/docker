CREATE DATABASE mysimplesql;

USE mysimplesql;

CREATE TABLE users (
    id INT NOT NULL PRIMARY KEY,
    f_name varchar(30),
    l_name varchar(30),
    email varchar(30)

)