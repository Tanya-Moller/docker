# We want Redhat/Centos Machine
# FROM is the base container that you'll build from
FROM centos:centos8.3.2011
# pinning your version is good practice

# We want httpd
## This is where we use base OS/BASH
RUN yum -y install httpd

# We want our own index file
## COPY ./local_dir /var/dir_container
COPY index.html /var/www/html/

# PORT for orchestration tool
    # Metadata is really important for Docker compose and Kubernetes
    # Metadat is data about something that is not something
    # example, the data on this docker file is all these lines of code. The METADAT is when was it created

## ENTRYPOINT

ENTRYPOINT ["httpd", "-D", "FOREGROUND"]