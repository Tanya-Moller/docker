# Docker

- Docker is a container tool that helps us build containers.

## Prerequisites
- Install Docker
- Have it working on the CLI

## Running a container

```sh
docker run -d -p 80:80 docker/getting-started

docker run [-options] owner_repo/image
```
- `-d` detached/daemon (run in background)
- `-p` port specify a mapping of a port (80:80 host:guest)
  - The host id is the physical machine running docker
  - The guest is the app you are running inside the container
- Right now it is running on port 80

Get a list of things that are running

```sh
docker ps 
```

### Changing the port
- e.g. to 7900


```sh
docker run -d -p 7900:80 docker/getting-started
```
- This makes a NEW machine, available on port 7900

### Remove a container

```sh
docker kill <container ID>
```

## Docker Hub

- What is Docker Hub?
- Find the hello-world docker image
- Download and have it running on port 80
- How do you give a running container a name?
  - Run another container with hello-world call it "Bazinga!" and make it run on port 4501

```sh
docker run --name Bazinga2 -p 4501:80 docker/hello-world
```

```
docker rm <name>
```

```
docker run --name NGINX -d -p 2021:80 -it nginx 
```

run container with `-dit`
- `d` is detached
- `t` is terminal
- creates log

```
docker run -dit -p 80:80 --name apachetan httpd
docker stop apachetan
docker start apachetan
```

## Passing variables to docker run

To pass variable, use a key=value and then the option `-e`


### Kill all containers
```
docker kill $(docker ps -q)
```

```docker run -e POSTGRES_PASSWORD=password -e POSTGRES_HOST_AUTH_METHOD=trust --name my_postgres -d postgres:latest```
- `-e VARIABLE=value` allows for variable input
- `postgres:latest` allows for specific versions

### Run commands inside the container
- To run inside the container, you use `exec`. There are some options.
- To keep the terminal open you pass `-ti`

```docker exec -it my_postgres psql -U postgres -W```

```sql
CREATE TABLE BOOKS (Title VARCHAR(64),  
Author VARCHAR(300))
;
```


### Docker Volumes
- Make data persistent
- They can be shared between containers
- You put your immutable code in the container, and the mutable part in an attached volume.
- Use the `-v`
- Specify the location in your `host:guest`

Syntax of -v
```
-v "path/local_dir:path/container_dir"
```

Example
```
docker run -e POSTGRES_PASSWORD=password -e POSTGRES_HOST_AUTH_METHOD=trust --name my_postgress -d -v "$PWD/volumes_postgress:/var/lib/postgress" postgres:latest 
```

```
touch ./volumes_postgress/hello.cat
echo "miau" >> ./volumes_postgress/hello.cat
```

### Docker Volume commands

```sh
docker volume ls
docker volume rm
docker volume prune # removes all the ones that arent in use
```

### Docker File

- It allows us to create our own containers from base containers.
- What type of OS is running out of the box docker httpd?
- Try running an exec command with a package manager

```sh
docker exec -it <container-name> apt
```
- it is debian and apache2

### Write a true httpd server from a centos machine and change our html welcome server

```sh
touch Dockerfile    
```
- Don't need the capital D - in older machines we do

### Docker File Commands
- FROM
- COPY
- RUN
- EXPOSE
- CMD & ENTRYPOINT

**EXPOSE** allows you to give metadata with regards to where your application/sw is running on what port. It's not the actual exposure of the port that is done by the software/program (in this case httpd).
- It signals that we have something running on port 80 for example

#### Building an Image from a Dockerfile
```sh
docker build -t <name> <directory>
```

then you can run 

```
docker run -dit --name -p 80:80 example_name new_image_name
```
If you have set your EXPOSE ports / metadata on which ports exist, then you can run with capital P

## Docker Compose

- Running 2 or more more containers inside the service.
- docker compose files are written in YAML.

```sh
docker-compose up
docker-compose down
```

### Example

```yml
version: "3.9"
services:
  web:
    build: .
    ports:
      - "5000:5000"
  redis:
    image: "redis:alpine"
```
- version of compose
- services is where you specify what you'll build
- Build means build the docker file in this repository
- Specify ports
- Get standard images from Docker.

Important things to include:
- Volumes
- You can reference services directly

## Docker Registry
- A service that stores and distributes images.
- Compares to Docker Hub but locally.

Start your registry

```sh
docker run -d -p 5000:5000 --name registry registry:2
```

Pull (or build) some image from the hub
```sh
docker pull ubuntu
```
Tag the image so that it points to your registry
```sh
docker image tag ubuntu localhost:5000/myfirstimage
```
Push it
```sh
docker push localhost:5000/myfirstimage
```
Pull it back
```sh
docker pull localhost:5000/myfirstimage
```
Now stop your registry and remove all data
```sh
docker container stop registry && docker container rm -v registry
```