from flask import Flask 
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

class Product(Resource):
    def get(self):
        return {
            'products': ['Orange Muffin', 'Plain Muffin', 'Blueberry Muffin']
        }

class Productprice(Resource):
    def get(self):
        return {
            'products': {'Orange Muffin': 10, 
            'Plain Muffin': 5, 
            'Blueberry Muffin': 15}
        }

api.add_resource(Product, '/')
api.add_resource(Productprice, '/price')

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)