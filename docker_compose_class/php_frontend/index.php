<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Vegan Muff</title>
</head>
<body>
    <h1>Welcome to our amazing Vegan Muffin Store</h1>
    <ul>
        <?php
            $json = file_get_contents('http://product-api');
            $objts = json_decode($json);

            $products = $objts->products;
            foreach ($products as $product) {
                echo "<li>$product</li>";
            }
        ?>

    </ul>
</body>
</html>